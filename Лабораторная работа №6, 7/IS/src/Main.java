import java.util.Scanner;

/**
 * Класс для вывода числа наоборот, которое ввёл пользователь, без 0 и 1
 *
 * @author M. Lebedev 17IT18
 */
public class Main {

    final static double MAX_VALUE = Math.pow(10, 8);
    final static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        long naturalUserNumber;

        try {
            naturalUserNumber = scanner.nextLong();
        } catch (Exception e) {
            System.out.println("Неверные_данные");
            return;
        }

        if (naturalUserNumber >= MAX_VALUE) {
            System.out.println("Неверные_данные");
            return;
        }

        long flipNumber = flip(naturalUserNumber);

        System.out.println(flipNumber);
    }

    /**
     * Метод переворачивает число пользователя без 0 и 1, и возвращает это значение
     *
     * @param naturalUserNumber число которое ввёл пользователь
     * @return перевёрнутое число пользователя без 0 и 1
     */
    public static long flip(long naturalUserNumber) {

        long flipNumber = 0;

        if (naturalUserNumber < 0) {
            naturalUserNumber *= -1;
        }

        while (naturalUserNumber >= 1) {

            long d = naturalUserNumber % 10;
            if (d > 1) {
                flipNumber = flipNumber * 10 + d;
            }
            naturalUserNumber = (naturalUserNumber - d) / 10;
        }

        return flipNumber;
    }
}
