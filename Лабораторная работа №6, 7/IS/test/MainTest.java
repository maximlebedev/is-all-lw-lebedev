import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class MainTest {

    /**
     * Тест на отрицательное число
     */
    @Test
    void negativeNumber() {
        long n = -1984;
        Assertions.assertEquals(489, Main.flip(n));
    }

    /**
     * Тест на только единицы и нули
     */
    @Test
    void allZeroOrOneDigit() {
        long n = 1010100101;
        Assertions.assertEquals(0, Main.flip(n));
    }

    /**
     * Тест на однозначное число
     */
    @Test
    void singleDigit() {
        long n = 5;
        Assertions.assertEquals(5, Main.flip(n));
    }

    /**
     * Тест на двузначое число
     */
    @Test
    void doubleDigit() {
        long n = 25;
        Assertions.assertEquals(52, Main.flip(n));
    }

    /**
     * Тест на трезначное число
     */
    @Test
    void threeDigit() {
        long n = 314;
        Assertions.assertEquals(43, Main.flip(n));
    }

    /**
     * Тест на четырехзначное число
     */
    @Test
    void fourDigit() {
        long n = 2442;
        Assertions.assertEquals(2442, Main.flip(n));
    }

    /**
     * Тест на пятизначное число
     */
    @Test
    void fiveDigit() {
        long n = 23456;
        Assertions.assertEquals(65432, Main.flip(n));
    }

    /**
     * Тест на шестизначное число
     */
    @Test
    void sixDigit() {
        long n = 123456;
        Assertions.assertEquals(65432, Main.flip(n));
    }

    /**
     * Тест на семизначное число
     */
    @Test
    void sevenDigit() {
        long n = 1234567;
        Assertions.assertEquals(765432, Main.flip(n));
    }

    /**
     * Тест на восьмизначное число
     */
    @Test
    void eightDigit() {
        long n = 1234560;
        Assertions.assertEquals(65432, Main.flip(n));
    }
}